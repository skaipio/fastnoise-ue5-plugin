// Copyright Epic Games, Inc. All Rights Reserved.

#include "FastNoiseUE5Plugin.h"

#define LOCTEXT_NAMESPACE "FFastNoiseUE5WrapperModule"

void FFastNoiseUE5WrapperModule::StartupModule()
{
	// This code will execute after your module is loaded into memory; the exact timing is specified in the .uplugin file per-module
}

void FFastNoiseUE5WrapperModule::ShutdownModule()
{
	// This function may be called during shutdown to clean up your module.  For modules that support dynamic reloading,
	// we call this function before unloading the module.
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FFastNoiseUE5WrapperModule, FastNoiseUE5Wrapper)